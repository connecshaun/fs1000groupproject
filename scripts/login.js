const form = document.querySelector('form');
const username = document.getElementById('username');
const password = document.getElementById('password');
const para = document.querySelector('p');

form.onsubmit = function(e) {

    if (username.value === '' || password.value === '') {
        e.preventDefault();
        para.textContent = 'You need to fill in both username and password!';
    }

    else if (password.value.length < 8) {
        e.preventDefault();
        para.textContent = 'The password must be at least 8 characters in length!'
    }

    else if (username.value === 'admin' && password.value === 'Admin123') {
        e.preventDefault();
        window.location.href="admin.html";
    }
    
    else {
        e.preventDefault();
        para.textContent = 'Authentication Failed!'
    }

    /*else if (pwWalidation.test(password.value)){
        e.preventDefault();
        para.textContent = 'The password must contain at least one lowercase and one uppercase and a number!'
    }*/
}
